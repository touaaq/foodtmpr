import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class SimpleFoodOrderingMachine {
    private JPanel root;
    private JButton tempraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane noOrderReceivedTextPane;
    void botan(String food){
        int confirmation= JOptionPane.showConfirmDialog(null, "Would you like to order?"+food, "Order Confirmation", JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            noOrderReceivedTextPane.setText("Order for");
            noOrderReceivedTextPane.setText(food);
            noOrderReceivedTextPane.setText("received");
            JOptionPane.showMessageDialog(null,"Order for" +food+ " received");


        }

    }



    public SimpleFoodOrderingMachine() {
        tempraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               botan("tempra");

        }
    });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                botan("Ramen");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                botan("Udon");
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new SimpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
